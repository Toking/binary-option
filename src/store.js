import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
    key: 'my-app',
    storage: localStorage
});

    export default new Vuex.Store({
      state: {
          currencies: [{}],
          deposit: 120
      },
      mutations: {
          setCurrencies(state,data) {
              state.currencies = data;
          },
          setRate(state,currency,data) {
              state.currencies[currency].rate_float = data;
          },
          setDeposit(state,data) {
              state.deposit = data;
          }

      },
      actions: {

      },
      plugins: [vuexPersist.plugin]
    })
